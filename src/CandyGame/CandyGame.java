package CandyGame;

import java.util.Random;
import java.util.Scanner;
/**
 * This is a class that creates and array and contains methods which are functions for the game.
 * 
 * @author Audrey Brennan and Matthew McAllister
 * Date: 2-4-2022
 * CS 235
 */


public class CandyGame {

	private int candyAmount[];
	private static final int LOWER = 15;
	private static final int UPPER = 30;
	private static final int CANDYLOWER = 4;
	private static final int CANDYUPPER = 10;
	private int[] classSize;

	public CandyGame() {

	}

	/**
	 * Gets integer from user within [15, 30] inclusive and returns number of students playing game.
	 * @param lowerLimit of number of students that can play.
	 * @param UpperLimit of number of students that can play.
	 * @return number of students playing the game.
	 */
	private int setClassSize(int lowerLimit, int upperLimit) {
		int studentNumber = 0;
		System.out.println("Choose the number of students");
		System.out.println("Enter a number between [15, 30] inclusive.");
		Scanner scan = new Scanner(System.in); 
		while(studentNumber == 0){
			int number = scan.nextInt();
			if(number < lowerLimit || number > upperLimit) {
				System.out.println("You must choose a number between [15, 30] inclusive.");
			}
			else{
				studentNumber = number;
			}
		}
		return studentNumber;
	}


	/**
	 * Gets user input, sets upper and lower limits, checks if its valid, and returns it. 
	 * @param lowerLimit - lowest integer that can be generated.
	 * @param UpperLimit - highest integer that can be generated.
	 * @return an even limit integer.
	 */
	private int setLimit(int lowerLimit, int upperLimit) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter an even number between " + lowerLimit + " and " + upperLimit + " inclusive:");
		boolean isValid = false;
		int temp = 0;
		while(isValid  == false) {
			int limit = scan.nextInt();
			if(limit >= lowerLimit && limit <= upperLimit && limit % 2 == 0) {
				temp = limit;
				isValid = true;
			}
			else {
				System.out.println("the number must be even and between " + lowerLimit + " and " + upperLimit + " inclusive");
			}
			
		}
		
		return temp;
	}


	/**
	 * Distributes random number of candy pieces to each section of the array(students) within the range.
	 * @param lowerLimit of starting pieces of candy
	 * @param UpperLimit of starting pieces of candy.
	 */
	private int[] distributeCandy(int lowerLimit, int upperLimit) {
		//iterate thru array of students and assign random value between highest and lowest limit
		Random rand = new Random();

		//classSize = new int[students];
		for(int i = 0; i < classSize.length; i++) {
			//int addedCandy = rand.nextInt(lowerLimit, upperLimit);
			boolean validCandy = false;
			while(validCandy == false) {
				int addedCandy = rand.nextInt(lowerLimit, upperLimit);
				if(addedCandy % 2 == 0) {
					classSize[i] = addedCandy;
					validCandy = true;
				}
			}
		}
	return classSize;
	}
	
	
	/**
	 * This method prints the contents of an array on one line. Each element gets four 
	 * character spaces.
	 * @param numCandy The array that holds the number of candies for each student
	 */
	public void printArray(int[] numCandy)
	{
		for(int student = 0; student < numCandy.length; student++)
		{
				System.out.printf("%4d", numCandy[student]);
		}
		System.out.println();
	}
	
	
	/**
	 * This method simulates passing half of a student's candy to the next student in line. Also,
	 * at the end of each pass, a candy is added to any student that has an odd number of candies.
	 * @param numCandy The int array that holds the amount of candy that each student holds
	 */
	public void passHalf(int[] numCandy) 
	{
		// Divide each index (number of Candy) by two
		for(int student = 0; student < (numCandy.length); student++)
		{
				// Divide each array index by two
				int half = numCandy[student] / 2;
				numCandy[student] = numCandy[student] / 2;
		}
			
		// Store the last index for later so it can be added to the first index
		int halfOfLast = numCandy[numCandy.length - 1];
			
		// Iterate backwards through the halved array, adding the number from the previous index.
		// Since the previous index is halved already, it's like adding half of the current to the next.
		for (int candy = numCandy.length - 1; candy >= 1; candy--)
		{
			numCandy[candy] = numCandy[candy] + numCandy[candy - 1];
			// If it's odd, add 1 to make it even
			if (numCandy[candy] % 2 == 1)
			{
				numCandy[candy]++;
			}
		}
			
		// Add half the last index (halfOfLast) to the first index
		numCandy[0] = numCandy[0] + halfOfLast;
		// If the first number is odd, add 1 to make it even
		if (numCandy[0] % 2 == 1)
		{
			numCandy[0]++;
		}
			
		//printArray(numCandy);
	}
	
	
	/**
	 * This method determines whether all elements within an int array are the same.
	 * @param numCandy The array that holds the number of candies for each student
	 * @return true If all elements within the array are the same.
	 */
	public boolean allSame(int[] numCandy)
	{
		for(int student = 1; student < numCandy.length; student++)
		{
			// Compare all array elements to the first. If any don't equal the first, return false.	
			if(numCandy[0] != numCandy[student])
				{
					return false;
				}
		}
		// Return true if all array elements pass the test above
		return true;
	}
	
		
	/**
	 * calls the methods to make the game run.
	 */
	public void run() {

		int candyNumber = setClassSize(LOWER, UPPER);
		classSize = new int[candyNumber];
		int lowLimit = setLimit(CANDYLOWER, CANDYUPPER);
		int highLimit = setLimit(lowLimit + 2, lowLimit + 50);
		//distributeCandy(lowLimit, highLimit);
		//int[] candy = new int[candyNumber];
		int[] candy = distributeCandy(lowLimit, highLimit);
		System.out.println("The original deal is: ");
		printArray(candy);
		System.out.println("We are ready to play the game.");
		System.out.println("Do you want to print the status after each move? (1 for yes, 0 for no)");
		Scanner scan = new Scanner(System.in);
		int printing = scan.nextInt();
		while (printing < 0 || printing > 1)
		{
			System.out.println("Please enter either 0 or 1!");
			printing = scan.nextInt();
		}
		
		boolean wantToPrint = true;
		if (printing == 0)
		{
			wantToPrint = false;
		}
		
		
		while(!allSame(candy))
		{
			passHalf(candy);
			if (wantToPrint)
			{
				printArray(candy);
			}
		}
		printArray(candy);
	}
}