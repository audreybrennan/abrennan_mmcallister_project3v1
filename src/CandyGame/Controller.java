package CandyGame;

/**
 * This is a class that creates and runs the CandyGame.
 * 
 * @author Audrey Brennan and Matthew McAllister
 * Date: 2-4-2022
 * CS 235
 */
public class Controller {

	public static void main(String[] args) {
		CandyGame candyGame = new CandyGame();
		candyGame.run();
	}

}
